# MenuDisplay

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Goal for this project
1. Open the application and run the project modify the application u can create new 
components based on your needs set up routes for the Single Page Application.

2. The routes should be "/home" for display basic information of the restaurant like address phone
number etc. Create another route "/category" and it's sub url "/category/:items" 
The "/category" will display all the categories in an UL list and "/category/:items" display items based on the
different category name. using table for display

Note: Here is the link for small demo U can check by this link
https://anatesan-stream.github.io/angularJS-restaurant-menu-app/module4_solution/#!/

Bonus: U can write the unit test cases for this application using Jasmine and Karma

Server Side: https://stream-restaurant-menu-svc.herokuapp.com/category  Get all the category info
https://stream-restaurant-menu-svc.herokuapp.com/item?category=SO    Get the items based on category

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
